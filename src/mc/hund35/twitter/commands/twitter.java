package mc.hund35.twitter.commands;


import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import mc.hund35.twitter.commands.handlers.notarget;
import mc.hund35.twitter.commands.handlers.placeholders;

public class twitter
  implements CommandExecutor, Listener
{

	public static String arg1;

  
  
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!(sender instanceof Player))
    {
      sender.sendMessage("§b§lTWITTER §8§l» §cError: this command cannot be executed from the console!");
      return true;
    }
    Player p = (Player)sender;
    if (commandLabel.equalsIgnoreCase("twitter")) {
      if (p.hasPermission("twitter.use"))
      {
        if (args.length == 0)
        {
        	p.sendMessage("§b§lTWITTER §8§l» §cWait: please wait to the gui have loaded  all the placeholders!");
            p.openInventory(notarget.GUI(p));
        	return false;
        }
        if(args.length >= 1){  
        	OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]);
        	if(target.hasPlayedBefore() == true){
        		p.sendMessage(placeholders.Liked(p));
        	}else{
        	sender.sendMessage("§b§lTWITTER §8§l» §cError: player (" + target.getName() + ") doesn't exists!");
        		return false;
        	}
      
        }
      }
    }
	return false;
  }
}