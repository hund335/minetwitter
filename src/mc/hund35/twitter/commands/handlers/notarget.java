package mc.hund35.twitter.commands.handlers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import mc.hund35.twitter.main;
import twitter4j.AsyncTwitter;
import twitter4j.AsyncTwitterFactory;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;

public class notarget
  implements Listener
{
  public static Inventory inv;
  private static String error = "&cCouldnt receive the data from twitter!";
  private static String error2 = "&cProbably because of twitter's receive limit for each 15 minute.";
  public static Twitter Twi;
  public static String followers_size;
  public static String liked;
  public static String following_size;
  public static String tweets_size;
  public static String lang;
  public static Inventory GUI(Player p)
  {

    File file = new File(main.instance.getDataFolder().getPath(), "guis/notarget.yml");
    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
    
	 File playerfile = new File(main.instance.getDataFolder().getPath(), "players/" + p.getUniqueId() + ".yml");
      YamlConfiguration pconfig = YamlConfiguration.loadConfiguration(playerfile);
      
      ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true).setOAuthConsumerKey(pconfig.getString("Player.ConsumerKey")).setOAuthConsumerSecret(pconfig.getString("Player.ConsumerSecret"))
		.setOAuthAccessToken(pconfig.getString("Player.AccessToken")).
		setOAuthAccessTokenSecret(pconfig.getString("Player.AccessTokenSecret"));
		AsyncTwitterFactory tf = new AsyncTwitterFactory(cb.build());
     AsyncTwitter twitter = tf.getInstance();
    
    try {
    	//User user = Twi.showUser(twitter.getId());
    	 User user = twitter.showUserData(twitter.getId());
    	 followers_size = String.valueOf(user.getFollowersCount());
    	 liked = String.valueOf(user.getFavouritesCount());
    	 following_size = String.valueOf(user.getFriendsCount());
    	 tweets_size = String.valueOf(user.getStatusesCount());
    	 lang = String.valueOf(user.getLang());
    	p.sendMessage("Followers: " + followers_size);
    	p.sendMessage("Liked: " + liked);
    	p.sendMessage("Following: " + following_size);
    	p.sendMessage("Tweets: " + tweets_size);
    	p.sendMessage("lang: " + lang);
   
    	
    	
    } catch (TwitterException e) {
    	e.printStackTrace();
    }
    
    p.sendMessage("Followers: " + followers_size);
	p.sendMessage("Liked: " + liked);
	p.sendMessage("Following: " + following_size);
	p.sendMessage("Tweets: " + tweets_size);
	p.sendMessage("lang: " + lang);
    
    inv = Bukkit.createInventory(null, config.getInt("GUI.Basic.Slots"), config.getString("GUI.Basic.Title").replace("&", "§"));
    Set<String> data2 = config.getConfigurationSection("GUI.Items").getKeys(false);
   for(String data : data2){
      ItemStack item = new ItemStack(Material.getMaterial(config.getInt("GUI.Items." + data + ".Item")),1,(short)config.getInt("GUI.Items." + data + ".Subid"));
      ItemMeta meta = item.getItemMeta();
      item.setAmount(config.getInt("GUI.Items." + data + ".Amount"));

        meta.setDisplayName(config.getString("GUI.Items." + data + ".Displayname").replace("&", "§"));
      
      if (config.getBoolean("GUI.Items." + data + ".Lore") == true)
      {
        List<String> lore = new ArrayList();
        List<String> loredata = config.getStringList("GUI.Items." + data + ".Lores");
       
  
        for (int i = 0; i < loredata.size(); i++)
        {
        	
       
	
	      

  
		

		lore.add(loredata.get(i).replace("&", "§")
				  .replace("%followers_size%", String.valueOf(followers_size))
				  .replace("%liked%", String.valueOf(liked)).
				   replace("%tweets_size%", String.valueOf(tweets_size))
				  .replace("%following_size%","" + String.valueOf(following_size))
				  .replace("%lang%", String.valueOf(lang)));

	     	
		
          meta.setLore(lore);
         
      }
      }
      item.setItemMeta(meta);
      inv.setItem(config.getInt("GUI.Items." + data + ".Slot"), item);
     

  }
    
    return inv;
  }
}