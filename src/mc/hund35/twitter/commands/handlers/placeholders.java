package mc.hund35.twitter.commands.handlers;

import java.io.File;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import mc.hund35.twitter.main;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class placeholders {

	private static String followers_size = "%followers_size%";
	private static String liked = "%liked%";
	private static String tweets_size = "%tweets_size%";
	private static String following_size = "%following_size%";
	private static String lang = "%lang%";
	
	public static String Followers(Player p){
		 File playerfile = new File(main.instance.getDataFolder().getPath(), "players/" + p.getUniqueId() + ".yml");
	        YamlConfiguration pconfig = YamlConfiguration.loadConfiguration(playerfile);
	        
	        ConfigurationBuilder cb = new ConfigurationBuilder();
			cb.setDebugEnabled(true).setOAuthConsumerKey(pconfig.getString("Player.ConsumerKey")).setOAuthConsumerSecret(pconfig.getString("Player.ConsumerSecret"))
			.setOAuthAccessToken(pconfig.getString("Player.AccessToken")).
			setOAuthAccessTokenSecret(pconfig.getString("Player.AccessTokenSecret"));
		
			 TwitterFactory tf = new TwitterFactory(cb.build());
	         Twitter twitter = tf.getInstance();
	         
	    
		
		try {
			return followers_size.replace("%followers_size%", followers_size.replace("%followers_size%", "" + twitter.showUser(twitter.getId()).getFollowersCount()));
		} catch (TwitterException e) {
		
			e.printStackTrace();
		}
		return followers_size.replace("%followers_size%", "error");
	}
	
	
	public static String Liked(Player p){
		 File playerfile = new File(main.instance.getDataFolder().getPath(), "players/" + p.getUniqueId() + ".yml");
	        YamlConfiguration pconfig = YamlConfiguration.loadConfiguration(playerfile);
	        
	        ConfigurationBuilder cb = new ConfigurationBuilder();
			cb.setDebugEnabled(true).setOAuthConsumerKey(pconfig.getString("Player.ConsumerKey")).setOAuthConsumerSecret(pconfig.getString("Player.ConsumerSecret"))
			.setOAuthAccessToken(pconfig.getString("Player.AccessToken")).
			setOAuthAccessTokenSecret(pconfig.getString("Player.AccessTokenSecret"));
		
			 TwitterFactory tf = new TwitterFactory(cb.build());
	         Twitter twitter = tf.getInstance();
	         
	      try {
			
	return liked.replace("%liked%", "" + twitter.showUser(twitter.getId()).getFavouritesCount());
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		
		return liked;
	}
	
	
	
	public static String Tweets(Player p){
		 File playerfile = new File(main.instance.getDataFolder().getPath(), "players/" + p.getUniqueId() + ".yml");
	        YamlConfiguration pconfig = YamlConfiguration.loadConfiguration(playerfile);
	        
	        ConfigurationBuilder cb = new ConfigurationBuilder();
			cb.setDebugEnabled(true).setOAuthConsumerKey(pconfig.getString("Player.ConsumerKey")).setOAuthConsumerSecret(pconfig.getString("Player.ConsumerSecret"))
			.setOAuthAccessToken(pconfig.getString("Player.AccessToken")).
			setOAuthAccessTokenSecret(pconfig.getString("Player.AccessTokenSecret"));
		
			 TwitterFactory tf = new TwitterFactory(cb.build());
	         Twitter twitter = tf.getInstance();
	         
	      try {
			return tweets_size.replace("%tweets_size%", "" + twitter.showUser(twitter.getId()).getStatusesCount());
	
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		
		return tweets_size;
	}
	
	
	
	public static String Following(Player p){
		 File playerfile = new File(main.instance.getDataFolder().getPath(), "players/" + p.getUniqueId() + ".yml");
	        YamlConfiguration pconfig = YamlConfiguration.loadConfiguration(playerfile);
	        
	        ConfigurationBuilder cb = new ConfigurationBuilder();
			cb.setDebugEnabled(true).setOAuthConsumerKey(pconfig.getString("Player.ConsumerKey")).setOAuthConsumerSecret(pconfig.getString("Player.ConsumerSecret"))
			.setOAuthAccessToken(pconfig.getString("Player.AccessToken")).
			setOAuthAccessTokenSecret(pconfig.getString("Player.AccessTokenSecret"));
		
			 TwitterFactory tf = new TwitterFactory(cb.build());
	         Twitter twitter = tf.getInstance();
	         
	      try {
			return following_size.replace("%following_size%","" + twitter.showUser(twitter.getId()).getFriendsCount());
	
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		
		return following_size;
	}
	
	
	
	public static String Lang(Player p){
		 File playerfile = new File(main.instance.getDataFolder().getPath(), "players/" + p.getUniqueId() + ".yml");
	        YamlConfiguration pconfig = YamlConfiguration.loadConfiguration(playerfile);
	        
	        ConfigurationBuilder cb = new ConfigurationBuilder();
			cb.setDebugEnabled(true).setOAuthConsumerKey(pconfig.getString("Player.ConsumerKey")).setOAuthConsumerSecret(pconfig.getString("Player.ConsumerSecret"))
			.setOAuthAccessToken(pconfig.getString("Player.AccessToken")).
			setOAuthAccessTokenSecret(pconfig.getString("Player.AccessTokenSecret"));
		
			 TwitterFactory tf = new TwitterFactory(cb.build());
	         Twitter twitter = tf.getInstance();
	         
	      try {
			return lang.replace("%lang%", "" + twitter.showUser(twitter.getId()).getLang());
	
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		
		return lang;
	}
	
	
	
	
}
