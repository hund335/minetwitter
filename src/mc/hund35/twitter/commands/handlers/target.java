package mc.hund35.twitter.commands.handlers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import mc.hund35.twitter.main;

public class target
  implements Listener
{
  public static Inventory inv;
  
  public static Inventory GUI()
  {

    File file = new File(main.instance.getDataFolder().getPath(), "guis/target.yml");
    YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
    inv = Bukkit.createInventory(null, config.getInt("GUI.Basic.Slots"), config.getString("GUI.Basic.Title").replace("&", "§"));
   
   Set<String> data = config.getConfigurationSection("GUI.Items").getKeys(false);

      ItemStack item = new ItemStack(Material.getMaterial(config.getInt("GUI.Items." + data + ".Item")));
      ItemMeta meta = item.getItemMeta();
      item.setAmount(config.getInt("GUI.Items." + data + ".Amount"));
      if (config.getBoolean("GUI.Items." + data + ".HasDisplayname")) {
        meta.setDisplayName(config.getString("GUI.Items." + data + ".Displayname").replace("&", "§"));
      }
      if (config.getBoolean("GUI.Items." + data + ".Lore"))
      {
        List<String> lore = new ArrayList();
        List<String> loredata = config.getStringList("GUI.Items." + data + ".Lores");
        for (int i = 0; i < loredata.size(); i++)
        {
          lore.add(loredata.get(i).replace("&", "§"));
          meta.setLore(lore);
        }
      }
      item.setItemMeta(meta);
      inv.setItem(config.getInt("GUI.Items." + data + ".Slot"), item);
  

      
    
    return inv;
  }
}