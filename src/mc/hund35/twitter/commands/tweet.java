package mc.hund35.twitter.commands;


import java.io.File;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import mc.hund35.twitter.main;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class tweet
  implements CommandExecutor, Listener
{

	public static String arg1;

  
  
  public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
  {
    if (!(sender instanceof Player))
    {
        sender.sendMessage("§b§lTWITTER §8§l» §cError: this command cannot be executed from the console!");
      return true;
    }
    Player p = (Player)sender;
    if (commandLabel.equalsIgnoreCase("tweet")) {
      if (p.hasPermission("tweet.use"))
      {
        if (args.length <= 0)
        {
        	    p.sendMessage("§b§lTWITTER §8§l» §cUsage: /tweet <tweet>");
        	return false;
        }
        if(args.length >= 1){  
        	arg1 = "";
       	for(int i = 0; i < args.length; i++){
        	arg1 += args[i] + " ";
       	}
        File file = new File(main.instance.getDataFolder().getPath(), "players/" + p.getUniqueId() + ".yml");
        YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
        
   		 ConfigurationBuilder cb = new ConfigurationBuilder();
   		cb.setDebugEnabled(true).setOAuthConsumerKey(config.getString("Player.ConsumerKey")).setOAuthConsumerSecret(config.getString("Player.ConsumerSecret"))
   		.setOAuthAccessToken(config.getString("Player.AccessToken")).
   		setOAuthAccessTokenSecret(config.getString("Player.AccessTokenSecret"));
   	
   		 TwitterFactory tf = new TwitterFactory(cb.build());
            Twitter twitter = tf.getInstance();
          

   		try {
   			twitter.updateStatus(arg1);
   			p.sendMessage("§b§lTWITTER §8§l» §aDu har nu posted dit tweet til twitter!");
   			
   			
   		} catch (TwitterException e) {
   			e.printStackTrace();
   			p.sendMessage("§b§lTWITTER §8§l» §aFejl: kunne ikke connect til kontoen!");
   		}
   		
        }
      }
    }
   		
    
    
	return false; 
  }
}
        
        